from collections import Counter
import numpy as np

trial_file = "CoNLL2009-ST-English-trial.txt"
train_file = "CoNLL2009-ST-English-train.txt"
evaluate_file = "CoNLL2009-ST-English-development.txt"
test_file = "test.csv"

ctp_size = 1

def process(file, vocabulary, vocab_size, word2id, label2id, ctp_size = 1, is_test = False):
    sentence = []
    words_, predicate_, ctx_p_, labels_, r_m_ = [], [], [], [], []
    data = {'words': words_, 'predicate': predicate_, 'ctx_p': ctx_p_, 'labels': labels_, 'r_m': r_m_}
    with open(file, encoding = "utf-8") as f:
        for line in f.readlines():
            line = line.strip().split()
            # check if line is empty
            if len(line) != 0:
                sentence.append(line)
            else:
                # loop through the sentence and derive the words and predicts
                predicates, pre_inds, words = [], [], []
                for j in range(len(sentence)):
                    if sentence[j][12] == 'Y':
                        predicate = sentence[j][13].split('.')[0]
                        if predicate in vocabulary:
                            predicates.append(predicate)
                        else:
                            predicates.append('unk')
                        pre_inds.append(j)
                    try:
                        words.append(word2id[sentence[j][2]])
                    except KeyError:
                        words.append(word2id['unk'])
                # "If a sequence has np predicates, we will process this sequence np times"
                for i in range(len(predicates)):
                    labels, ctx_p = [], []
                    r_m = [0]*len(sentence)
                    start = pre_inds[i] - ctp_size
                    end = pre_inds[i] + ctp_size + 1
                    for k in range(start, end):
                        if k >= 0 and k < len(sentence):
                            r_m[k] = 1
                            try:
                                ctx_p.append(word2id[sentence[k][2]])
                            except KeyError:
                                ctx_p.append(vocab_size)
                        else:
                            # corresponding to the zero word embedding
                            ctx_p.append(vocab_size)
                    for l in range(len(sentence)):
                        if is_test is True:
                            labels.append(label2id['_'])
                        else:
                            labels.append(label2id[sentence[l][14 + i]])

                    words_.append(words)
                    predicate_.append(word2id[predicates[i]])
                    ctx_p_.append(ctx_p)
                    labels_.append(labels)
                    r_m_.append(r_m)
                # reset the sentence
                sentence = []
    return data

def build_dict(file):
    lemma = []
    arguments = []
    word2id = {}
    label2id = {}
    with open(file, encoding = "utf-8") as f:
        for line in f:
            line = line.strip().split()
            if len(line) != 0:
                lemma.append(line[2])
                arguments.extend(line[14:])
    lemma.append('unk')
    for word, _ in (Counter(lemma)).items():
        word2id[word] = len(word2id)
    #reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    for label, _ in (Counter(arguments)).items():
        label2id[label] = len(label2id)
    id2label = dict(zip(label2id.values(), label2id.keys()))

    vocabulary = list(word2id.keys())
    vocab_size = len(vocabulary)
    argu_size = len(list(label2id.keys()))

    return vocabulary, vocab_size, word2id, label2id, id2label, argu_size

def build_embeddings(vocabulary, vocab_size):
    data = {}
    unfound = []
    embedding_size = 50
    # read the embedding file and convert it to an embedding dictionary
    with open('glove.6B.50d.txt', encoding="utf-8") as f:
        for line in f:
            tokens = line.rstrip().split(' ')
            data[tokens[0]] = np.array(tokens[1:], dtype = np.float32)
    # create specfic word embeddings for dataset
    embeddings = np.zeros([vocab_size+1, embedding_size], dtype=np.float32)
    # obtain embeddings from glove word embeddings and count the number of words excluded
    num_not_found = 0
    for i in range(len(vocabulary)):
        if vocabulary[i] in data:
            embeddings[i, :] = data[vocabulary[i]]
        else:
            num_not_found += 1
            embeddings[i, :] = np.random.normal(0.0, 0.1, embedding_size)
    print('n words not found in glove word vectors: ' + str(num_not_found))

    return embeddings

def nextbatch(dataset, start_index, batch_size, vocab_size):
    max_len = 0
    predicates, ctx_ps, actual_length = [], [], []
    last_index = start_index + batch_size
    words = dataset['words'][start_index:last_index]
    labels = dataset['labels'][start_index:last_index]
    predicate = dataset['predicate'][start_index:last_index]
    ctx_p = dataset['ctx_p'][start_index:last_index]
    r_m = dataset['r_m'][start_index:last_index]
    for i in range(batch_size):
        length = len(words[i])
        actual_length.append(length)
        if length > max_len:
            max_len = length
        predicates.append([predicate[i]]*length)
        ctx_ps.append([ctx_p[i]]*length)
        #if length > sequence_length:
        #    actual_length.append(sequence_length)
        #else:
        #    actual_length.append(length)
    words_padded = pad(words, max_len, vocab_size)
    predicates_padded = pad(predicates, max_len, vocab_size)
    ctx_ps_padded = pad_ctx(ctx_ps, max_len, vocab_size, ctp_size)
    labels_padded = pad(labels, max_len, 0)
    r_m_padded = pad(r_m, max_len, 0)

    return words_padded, predicates_padded, ctx_ps_padded, labels_padded, r_m_padded, actual_length, max_len

def pad(data, maxlen, value):
    padded = np.full((len(data),maxlen),value)
    for i in range(len(data)):
        sequence_length = len(data[i])
        padded[i][0:sequence_length] = data[i]
    return padded

def pad_ctx(data, maxlen, value, ctp_size = 1):
    padded = np.full((len(data), maxlen, ctp_size*2+1), value)
    for i in range(len(data)):
        sequence_length = len(data[i])
        padded[i][0:sequence_length] = data[i]
    return padded