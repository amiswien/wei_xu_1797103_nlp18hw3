pred_file, test_file, dest_file = "preds_9.txt", "test.txt", "test_results.txt"
sentence, preds, content  = [], [], []
null = ['null']
with open(pred_file, encoding='utf-8') as p:
    for line in p.readlines():
        line_ = line.strip().lstrip('[').rstrip(']').replace('\'','').split(', ')
        preds.extend(line_)
with open(test_file, encoding='utf-8') as f:
    for line in f.readlines():
        line = line.strip().split()
        if len(line) != 0:
            sentence.append(line)
        else:
            predicate_num = 0
            leng = len(sentence)
            for token in sentence:
                if token[12] == 'Y':
                    predicate_num += 1
            if predicate_num == 0:
                content.extend([null]*leng)
            else:
                labels = preds[0:leng*predicate_num]
                del preds[0:leng*predicate_num]
                for j in range(leng):
                    con_temp = []
                    for k in range(predicate_num):
                        con_temp.append(labels[j+k*leng])
                    content.append(con_temp)
            sentence = []
    with open(test_file, 'r') as src:
        with open(dest_file, 'w') as dest:
            index = 0
            for line in src:
                if line == '\n':
                    print('', file = dest)
                else:
                    line = line.rstrip()
                    for argu in content[index]:
                        line += '	{}'.format(argu)
                    print(line, file = dest)
                    index += 1
