from preprocess import *
import numpy as np
import tensorflow as tf

trial_file = "CoNLL2009-ST-English-trial.txt"
train_file = "CoNLL2009-ST-English-train.txt"
evaluate_file = "CoNLL2009-ST-English-development.txt"
test_file = "test.csv"

# set parameters
embedding_size = 50
learning_rate = 0.002
hidden_size = 300
batch_size = 32
num_of_epochs = 1
keep_prop = 0.5
#sequence_length = 50
ctp_size = 1

vocabulary, vocab_size, word2id, label2id, id2label, num_classes = build_dict(train_file)
train_data = process(train_file, vocabulary, vocab_size, word2id, label2id, ctp_size)
val_data = process(evaluate_file, vocabulary, vocab_size, word2id, label2id, ctp_size)
test_data = process(test_file, vocabulary, vocab_size, word2id, label2id, ctp_size, is_test = True)

embeddings = build_embeddings(vocabulary, vocab_size)

g = tf.Graph()
with g.as_default():
    with tf.name_scope('inputs'):
        words = tf.placeholder(tf.int32, shape=[batch_size, None], name='words')
        predicates = tf.placeholder(tf.int32, shape=[batch_size, None], name='predicts')
        ctx_p = tf.placeholder(tf.int32, shape=[batch_size, None, ctp_size*2+1], name='predict_context')
        labels = tf.placeholder(tf.int32, shape=[batch_size, None], name='labels')
        r_m = tf.placeholder(tf.int32, shape=[batch_size, None], name='r_m')
        length = tf.placeholder(tf.int32, shape=[batch_size], name='length')

        word_embeddings = tf.Variable(embeddings, dtype=tf.float32, trainable=False, name='embeddings')

        words_emb = tf.nn.embedding_lookup(word_embeddings, words)
        predicates_emb = tf.nn.embedding_lookup(word_embeddings, predicates)
        ctx_p_emb = tf.reshape(tf.nn.embedding_lookup(word_embeddings, ctx_p), [batch_size, -1, embedding_size*(ctp_size*2+1)])
        inputs_emb = tf.reshape(tf.concat([words_emb, predicates_emb, ctx_p_emb], 2), [batch_size, -1, embedding_size*(ctp_size*2+3)])
        r_m_ = tf.expand_dims(tf.to_float(r_m), 2)
        inputs_emb = tf.concat([inputs_emb, r_m_], 2)

    with tf.variable_scope("bilstm"):

        # lstm cell
        lstm_cell_fw = [tf.nn.rnn_cell.DropoutWrapper(tf.nn.rnn_cell.BasicLSTMCell(hidden_size),
                                                      input_keep_prob=keep_prop) for i in range(1)]
        lstm_cell_bw = [tf.nn.rnn_cell.DropoutWrapper(tf.nn.rnn_cell.BasicLSTMCell(hidden_size),
                                                      input_keep_prob=keep_prop) for i in range(1)]


        lstm_cell_fw = tf.nn.rnn_cell.MultiRNNCell(lstm_cell_fw)
        lstm_cell_bw = tf.nn.rnn_cell.MultiRNNCell(lstm_cell_bw)

        #cell_fw = tf.nn.rnn_cell.DropoutWrapper(tf.nn.rnn_cell.BasicLSTMCell(hidden_size), input_keep_prob=keep_prop)
        #cell_fw = tf.nn.rnn_cell.MultiRNNCell([cell_fw]*4)
        #cell_bw = tf.nn.rnn_cell.DropoutWrapper(tf.nn.rnn_cell.BasicLSTMCell(hidden_size), input_keep_prob=keep_prop)
        #cell_bw = tf.nn.rnn_cell.MultiRNNCell([cell_bw]*4)
        outputs, _ = tf.nn.bidirectional_dynamic_rnn(lstm_cell_fw,
            lstm_cell_bw, inputs_emb, dtype=tf.float32)
        outputs = tf.reshape(tf.concat(axis=2, values=outputs), [-1, hidden_size * 2])

    with tf.variable_scope("output"):
        W = tf.get_variable(name="W", shape=[hidden_size * 2, num_classes],
                            initializer=tf.truncated_normal_initializer(stddev=0.05),
                            dtype=tf.float32)
        b = tf.get_variable(name="b", shape=[num_classes],
                            initializer=tf.constant_initializer(value=0.),
                            dtype=tf.float32)
        logits = tf.matmul(outputs, W) + b
        logits = tf.reshape(logits, [batch_size, -1, num_classes])

    with tf.name_scope("loss"):
        # Compute the log-likelihood of the gold sequences and keep the transition
        # params for inference at test time.
        log_likelihood, trans_params = tf.contrib.crf.crf_log_likelihood(
            logits, labels, length)
        loss = tf.reduce_mean(-log_likelihood)

        # Compute the viterbi sequence and score.
        viterbi_sequence, viterbi_score = tf.contrib.crf.crf_decode(
            logits, trans_params, length)

    tf.summary.scalar('loss', loss)

    with tf.name_scope("optimizer"):
        train_op = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(loss)

    merged = tf.summary.merge_all()

    saver = tf.train.Saver()

with tf.Session(graph=g) as sess:
    writer = tf.summary.FileWriter('tmp/', sess.graph)
    sess.run(tf.global_variables_initializer())

    epochs = 5
    steps_per_epoch = int(len(train_data['predicate']) / batch_size)
    steps = epochs * steps_per_epoch
    for i in range(steps):

        words_padded, predicates_padded, ctx_ps_padded, labels_padded, r_m_padded, actual_length, max_len = \
            nextbatch(train_data, (i % steps_per_epoch)*32, batch_size, vocab_size)

        tf_viterbi_sequence, tf_loss, _ = sess.run([viterbi_sequence, loss, train_op], \
            feed_dict={words: words_padded, predicates: predicates_padded, \
            ctx_p: ctx_ps_padded, labels: labels_padded, r_m: r_m_padded, length: actual_length})

        if i % 1000 == 0:
            print(i)
            print('loss:{}'.format(tf_loss))
            mask = (np.expand_dims(np.arange(max_len), axis=0) < np.expand_dims(actual_length, axis=1))
            total_labels = np.sum(actual_length)
            correct_labels = np.sum((labels_padded == tf_viterbi_sequence) * mask)

            accuracy = 100.0 * correct_labels / float(total_labels)
            print("Accuracy: %.2f%%" % accuracy)

        if i == steps - 1:
            iterations = int(len(test_data['predicate']) / batch_size)
            f = open('preds_9.txt', 'w')
            for j in range(iterations+1):
                start_index = j * 32
                if j == iterations:
                    start_index = start_index - 5 #10

                preds = []

                #words_padded_val, predicates_padded_val, ctx_ps_padded_val, labels_padded_val, r_m_padded_val, actual_length_val, max_len_val = \
                #    nextbatch(val_data, start_index, batch_size, vocab_size)
                words_padded_test, predicates_padded_test, ctx_ps_padded_test, labels_padded_test, r_m_padded_test, actual_length_test, max_len_test = \
                    nextbatch(test_data, start_index, batch_size, vocab_size)

                #tf_viterbi_sequence_, _ = sess.run([viterbi_sequence, train_op], \
                #    feed_dict={words: words_padded_val, predicates: predicates_padded_val, \
                #    ctx_p: ctx_ps_padded_val, labels: labels_padded_val, r_m: r_m_padded_val, length: actual_length_val})
                tf_viterbi_sequence_, _ = sess.run([viterbi_sequence, train_op], \
                    feed_dict={words: words_padded_test, predicates: predicates_padded_test, \
                    ctx_p: ctx_ps_padded_test, labels: labels_padded_test, r_m: r_m_padded_test, length: actual_length_test})

                for i in range(len(tf_viterbi_sequence_)):
                    preds.append(tf_viterbi_sequence_[i][0:actual_length_test[i]])

                if j == iterations:
                    preds = preds[5:] #10

                for pred in preds:
                    pred = [id2label[label] for label in pred]
                    f.write("%s\n" % pred)
            f.close()

    # save the model and graph
    saver.save(sess, 'tmp/srl_model')
    writer.close()
