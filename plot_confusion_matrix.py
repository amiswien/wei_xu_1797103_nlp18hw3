import seaborn as sn
import pandas as pd
import matplotlib.pyplot as plt

array = [[6950,3044],[6915,181164]]
df_cm = pd.DataFrame(array, range(2),
                  range(2))
#plt.figure(figsize = (10,7))
sn.set(font_scale=1.4)#for label size
sn.heatmap(df_cm, annot=True,annot_kws={"size": 16})# font size
plt.savefig('myfig')
plt.show()

