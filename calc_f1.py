import sys, os

def calc_f1(pred_file, gold_file):
    # case_true = TP, case_recall = TP + FN, case_precision = TP + FP
    case_true, case_recall, case_precision, tn1, tn2 = 0, 0, 0, 0, 0
    golds, preds, sentence = [], [], []
    # process gold file
    with open(gold_file, encoding = "utf-8") as g:
        for line in g.readlines():
            line = line.strip().split()
            if len(line) != 0:
                sentence.append(line)
            else:
                predicts_num = len(sentence[0]) - 14
                for i in range(predicts_num):
                    for word in sentence:
                        argu = word[14+i]
                        if argu is '_':
                            tn1 += 1
                        elif argu is not '_':
                            case_recall += 1
                        golds.append(argu)
                sentence = []
    # process pred file
    with open(pred_file, encoding='utf-8') as p:
        for line in p.readlines():
            line_ = line.strip().lstrip('[').rstrip(']').replace('\'','').split(', ')
            preds.extend(line_)
        for argu in preds:
            if argu is '_':
                tn2 +=1
            elif argu is not '_':
                case_precision += 1
    assert len(golds) == len(preds), "length of prediction file and gold file should be the same."

    for i in range(len(golds)):
        if preds[i] != '_' and preds[i] == golds[i]:
            case_true += 1
    assert case_recall != 0, "no labels in gold files!"
    assert case_precision != 0, "no labels in pred files!"
    recall = 1.0 * case_true / case_recall
    precision = 1.0 * case_true / case_precision
    f1 = 2.0 * recall * precision / (recall + precision)
    print(tn1, tn2)
    print(case_true, case_recall, case_precision)
    result = "recall: %s  precision: %s  F1: %s" % (str(recall), str(precision), str(f1))
    return result
# calc_f1
if __name__ == "__main__":
    print(calc_f1('preds_3.txt', 'CoNLL2009-ST-English-development.txt'))